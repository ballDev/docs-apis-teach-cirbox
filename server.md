# Apis Cirbox Teach rev. 1.0

- baseUrl : `http://xxx.xxx.x.x:7000/cirbox-front-teach`

## Revision History 

| Version | Date | Changes made |
|---------|------|--------------|
|1.0.0|2020-03-20|First Draft|

### API Get Product list
- Path : `{baseUrl}/product`
- Method : `GET`
- Response `<application/json>`

    ```json
    {
        "resCode": "0000|1000",
        "resData": [
            {
                "productId": "string",
                "productName": "string",
                "productDesc": "string",
                "imageUrl": "string",
                "price": 0,
            }
        ]
    }
    ```


### API Create Product
- Path : `{baseUrl}/product/create`
- Method : `POST`
- Request `<application/json>`

    ```json
    {
        "productName": "string",
        "productDesc": "string",
        "imageUrl": "string",
        "price": 0,
    }
    ```
- Response `<application/json>`
    ```json
    {
        "resCode": "0000|1000",
        "resData": {
            "productId": "string",
            "productName": "string",
            "productDesc": "string",
            "imageUrl": "string",
            "price": 0,
        }
    }
    ```


### API Update Product
- Path : `{baseUrl}/product/update/{productId}`
- Method : `PUT`
- Request `<application/json>`

    ```json
    {
        "productName": "string",
        "productDesc": "string",
        "imageUrl": "string",
        "price": 0,
    }
    ```
- Response `<application/json>`
    ```json
    {
        "resCode": "0000|1000",
        "resData": {
            "productId": "string",
            "productName": "string",
            "productDesc": "string",
            "imageUrl": "string",
            "price": 0,
        }
    }
    ```


### API Delete Product
- Path : `{baseUrl}/product/delete/{productId}`
- Method : `DELETE`
- Response `<application/json>`

    ```json
    {
        "resCode": "0000|1000",
        "resData": {}
    }
    ```


### API Get Stock List
- Path : `{baseUrl}/stock`
- Method : `GET`
- Response `<application/json>`

    ```json
    {
        "resCode": "0000|1000",
        "resData": [ 
            {
                "productId": "string",
                "stockQty": 0
            }
        ]
    }
    ```

### API Update Stock
- Path : `{baseUrl}/stock/update/{productId}`
- Method : `PUT`
- Request `<application/json>`

    ```json
    {
        "stockQty": 0
    }
    ```
- Response `<application/json>`
    ```json
    {
        "resCode": "0000|1000",
        "resData": {
            "productId": "string",
            "stockQty": 0
        }
    }
    ```


---

| resCode | Status |
|---------|------|
|0000|`Success`|
|1000|`UnSuccess`|